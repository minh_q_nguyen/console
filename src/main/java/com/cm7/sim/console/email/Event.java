package com.cm7.sim.console.email;

public enum Event {

    PROCESS_SCAN_INFECTION_FOUND("infection_found", "Daily malware found"), REPEATED_THREAT_INFECTION_FOUND(
        "infection_found", "Device reported threats"), IP_SCANNING("infection_found",
            "Daily suspicious IP connections found"), INFECTION_CLEARED("infection_cleared",
                "Device reported no more threats"),

    ISSUE_FOUND("issue_found", "Device changed status to non-compliant"), ISSUE_CLEARED("issue_cleared",
        "Device changed status to compliant"),

    CRITICAL_FOUND("critical_issue_found", "Device reported critical issues"), CRITICAL_CLEARED(
        "critical_issue_clear", "Device reported no more critical issues"),

    DEVICE_ADDED("added", "Device added"), DEVICE_DELETED("deleted",
        "Device removed by an administrator"), DEVICE_UNINSTALLED("uninstall", "Device uninstalled by user"),

    UNSEEN_REMOVED("unseen_removed", "Device deleted by unseen setting"), DEVICE_OUTBREAK("device_outbreak",
        "Device Outbreak"),

    CONTACT_IT("contact_it", "Contact IT"),

    LOGGED_ON("logged_on", "Admin logged on"), LOGGED_OFF("logged_off", "Admin logged off"), CHANGED_CONFIG(
        "config_change", "Admin made configuration change"), CHANGED_CAC("config_change",
            "Admin changes Cloud Access Control setting"), EXEMPT_DEVICE("exempt_device",
                "Device exempted"), UNEXEMPT_DEVICE("unexempt_device", "Device unexempted"),

    ALLOW_DEVICE_ACCESS("allow_access", "Device allowed access"), BLOCK_DEVICE_ACCESS("block_access",
        "Device blocked access"), GRANT_DEVICE_ACCESS("grant_access",
            "Device granted temporary access"), REVOKE_DEVICE_ACCESS("revoke_access",
                "Device revoked temporary access"),

    INVITE_USER("invite_user", "[OPSWAT Central Management] Account Invitation"), INVITE_USER_NOTI(
        "invite_user_noti", "[OPSWAT Central Management] Account Invitation"),

    ALERT_NEW_ACCOUNT("alert_new_account", "alert_new_account"), REGCODE_MOBILE("regcodemobile",
        "regcodemobile"), NEW_PIN("new_pin", "New Pin"),

    ACCOUNT_SUMMARY_REPORT("account_summary_report", "Account summary report"),

    ACCOUNT_OUT_OF_LICENSE("account_out_of_license", "Account is running out of license"),

    REPORT_ACTIVE_USERS_WEEKLY("report_active_users_weekly", "REPORT_ACTIVE_USERS_WEEKLY"),

    DUMP_ACCOUNT_DONE("report_active_users_weekly",
        "DUMP_ACCOUNT_DONE"), DUMP_ACCOUNT_STARTED("dump_account_started", "DUMP_ACCOUNT_STARTED"),

    CONTACT_SALES_FOR_UPGRADE_ACCOUNT("contact_sales_for_upgrade_account",
        "CONTACT_SALES_FOR_UPGRADE_ACCOUNT"), CONTACT_ADMIN_FOR_UPGRADE_ACCOUNT(
            "contact_admin_for_upgrade_account",
            "CONTACT_ADMIN_FOR_UPGRADE_ACCOUNT"), CONTACT_SALES_EXTEND_DATA_RETENTION(
                "contact_sales_extend_data_retention", "CONTACT_SALES_EXTEND_DATA_RETENTION");

    private String key;
    private String value;

    private Event(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String toString() {
        return value;
    }

    public String getKey() {
        return key;
    }

    public boolean isExemptEvent() {
        return this == EXEMPT_DEVICE || this == UNEXEMPT_DEVICE;
    }

    public boolean isInfectionType() {
        return this == PROCESS_SCAN_INFECTION_FOUND || this == REPEATED_THREAT_INFECTION_FOUND
            || this == IP_SCANNING;
    }

    public boolean isRepeatedThreatInfectionFound() {
        return this == REPEATED_THREAT_INFECTION_FOUND;
    }

    public boolean isDeviceCriticalFound() {
        return this == Event.CRITICAL_FOUND;
    }

    public boolean isDeviceChangeStatus() {
        return this == ISSUE_FOUND || this == ISSUE_CLEARED;
    }

    public boolean isAdminEvent() {
        Event[] adminEvent = { LOGGED_ON, LOGGED_OFF, CHANGED_CONFIG, CHANGED_CAC };
        for (int i = 0; i < adminEvent.length; i++) {
            if (adminEvent[i] == this) {
                return true;
            }
        }
        return false;
    }

    public boolean isDeviceAccess() {
        return this == ALLOW_DEVICE_ACCESS || this == BLOCK_DEVICE_ACCESS;
    }

    public boolean isByPassAccessGranted() {
        return this == GRANT_DEVICE_ACCESS;
    }

    public boolean isByPassAccessRevoked() {
        return this == REVOKE_DEVICE_ACCESS;
    }

}
