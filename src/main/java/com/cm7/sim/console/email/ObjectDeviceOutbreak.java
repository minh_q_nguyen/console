package com.cm7.sim.console.email;

import java.util.Map;

public class ObjectDeviceOutbreak extends ObjectModelBased {

    private Map<String, Object> model;

    public ObjectDeviceOutbreak(Map<String, Object> model) {
        this.eventType = Event.DEVICE_OUTBREAK;
        this.model = model;
        this.receivers = new String[] { "minh.q.nguyen@opswat.com" };
    }

    @Override
    public Map<String, Object> createTemplateModel() {
        return this.model;
    }

    @Override
    public String getSubject() {
        return "[OPSWAT Central Management] Devices status outbreak alert";
    }

}
