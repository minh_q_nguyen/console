package com.cm7.sim.console.email;

import java.util.Map;

public abstract class ObjectEmail {

    protected String[] receivers;
    protected String template;

    public abstract String getSubject();

    public abstract String getContent();

    public String[] getBcc() {
        return null;
    }

    public Map<String, String> getImagesInline() {
        return null;
    }

    public String[] getReceivers() {
        return receivers;
    }

    public void setReceivers(String[] receivers) {
        this.receivers = receivers;
    }
}
