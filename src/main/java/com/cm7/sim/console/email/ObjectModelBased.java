package com.cm7.sim.console.email;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.util.StringUtils;

public abstract class ObjectModelBased extends ObjectEmail {

    protected Event eventType;
    protected Date time;
    protected String groupName = "";
    protected String accountId;
    protected String adminName;
    protected List<String> logs = new ArrayList<>();

    public ObjectModelBased() {
        this.time = new Date();
    }

    public abstract Map<String, Object> createTemplateModel();

    public String getAccountId() {
        return accountId;
    }

    public Event getEventType() {
        return eventType;
    }

    public String getGroupName() {
        return StringUtils.isEmpty(groupName) ? "" : groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setLogs(List<String> logs) {
        this.logs = logs;
    }

    private static String getStringDateInTimezone(Date date, String dateFormat, String timezone) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setTimeZone(TimeZone.getTimeZone(timezone));
        return sdf.format(date);
    }

    protected String getTimeInString() {
        return getStringDateInTimezone(time, "MMM dd, yyyy hh:mm a", "UTC") + " (UTC)";
    }

    protected String getTimeInString(String format) {
        return getStringDateInTimezone(time, format, "UTC");
    }

    public String getFilePath() {
        if (eventType == null)
            return "";

        if (eventType == Event.DEVICE_OUTBREAK)
            return "device_outbreak.html";

        return "device_action.html";
    }

    @Override
    public String getContent() {
        Map<String, Object> model = this.createTemplateModel();

        // if template is load from db
        if (StringUtils.hasText(template)) {
            VelocityContext context = new VelocityContext(model);
            StringWriter writer = new StringWriter();
            Velocity.evaluate(context, writer, "", super.template);
            return writer.toString();
        }

        String filePath = this.getFilePath();
        filePath = String.format("email-template/%s", filePath);
        Template template = getVelocityEngine().getTemplate(filePath);
        VelocityContext context = new VelocityContext(model);
        StringWriter writer = new StringWriter();
        template.merge(context, writer);

        return writer.toString();
    }

    private VelocityEngine getVelocityEngine() {
        VelocityEngine ve = new VelocityEngine();
        ve.setProperty("resource.loader", "class");
        ve.setProperty(
            "class.resource.loader.class",
            "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        ve.init();
        return ve;
    }
}
