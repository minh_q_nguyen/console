package com.cm7.sim.console.controler;

import com.cm7.sim.console.service.impl.Cm8Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cm8")
public class Cm8Controller {

    @Autowired
    Cm8Service cm8Service;

    @GetMapping
    public String getCm8Token() {
        // var token = cm8Service.postToken();
        // System.out.println("\u001B[33m post token = '" + token + "' \u001B[0m");
        return cm8Service.getToken();
    }

}
