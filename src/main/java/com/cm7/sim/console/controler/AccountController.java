package com.cm7.sim.console.controler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.cm7.sim.console.service.AccountService;
import com.cm7.sim.domain.model.SimAccountDto;
import com.cm7.sim.domain.model.request.LoginRequest;
import com.cm7.sim.domain.repository.entity.SimAccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/accounts")
public class AccountController {

    @Autowired
    AccountService accountService;

    @GetMapping("/test/name")
    public String getUserName() {
        return "Bruce Wayne";
    }

    @PostMapping
    public SimAccount createAccount(@Valid @RequestBody SimAccountDto req) {
        var auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            System.out.println("\u001B[33m mmm### 'Youre auth is null' \u001B[0m");
        }
        return accountService.createAccount(req);
    }

    @DeleteMapping
    public String deleteAccount(@RequestBody String id) {
        return accountService.deleteAccountById(id);
    }

    @PostMapping(
        value = "/login",
        produces = "application/json; charset=UTF-8",
        consumes = "application/json; charset=UTF-8")
    @ResponseBody
    public String login(HttpSession session, @Valid @RequestBody LoginRequest req, HttpServletRequest request,
        HttpServletResponse response) {
        return accountService.login(req, session, request);
    }

}
