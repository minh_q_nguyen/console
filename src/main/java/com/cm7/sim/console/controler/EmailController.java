package com.cm7.sim.console.controler;

import java.util.Date;
import java.util.HashMap;
import com.cm7.sim.console.email.ObjectDeviceOutbreak;
import com.cm7.sim.console.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/emails")
public class EmailController {

    @Autowired
    MailService mailService;

    @GetMapping
    public void sendEmail() throws Exception {
        var model = new HashMap<String, Object>();
        model.put("percentage", 20);
        model.put("non_compliant_count", 14);
        model.put("time", new Date().toString());
        model.put("server_url", "https://localhost");
        var mail = new ObjectDeviceOutbreak(model);
        mailService.sendEmail(mail);
    }

}
