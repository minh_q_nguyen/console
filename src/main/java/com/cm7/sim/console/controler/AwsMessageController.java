package com.cm7.sim.console.controler;

import com.cm7.sim.console.service.impl.AwsMessageQueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("aws-message")
public class AwsMessageController {

    @Autowired
    AwsMessageQueueService awsMessageQueueService;

    @PostMapping
    public String sendMessage() {
        awsMessageQueueService.sendMessageCallback();
        return "done!";
    }

}
