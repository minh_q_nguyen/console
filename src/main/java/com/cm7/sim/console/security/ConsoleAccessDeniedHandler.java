package com.cm7.sim.console.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;

public class ConsoleAccessDeniedHandler extends AccessDeniedHandlerImpl {

    @Override
    public void handle(HttpServletRequest req, HttpServletResponse res, AccessDeniedException exception)
            throws IOException, ServletException {
        if (!res.isCommitted()) {
            if (!isLogged(req) && isAjaxRequest(req)) {
                String result = createFailedJSONResponse("notlogin").toString();
                res.getWriter().write(result);
                res.setContentType("application/json");
                return;
            }
            super.handle(req, res, exception);
        }
    }

    private boolean isLogged(HttpServletRequest req) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        HttpSession session = req.getSession(false);
        return auth != null && session != null ? true : false;
    }

    private boolean isAjaxRequest(HttpServletRequest req) {
        boolean isAjax = req.getHeader("Accept").contains("application/json");
        return isAjax;
    }

    private static JSONObject createFailedJSONResponse(String... params) {
        JSONObject response = new JSONObject();
        try {
            response.put("status", "false");
            if (params.length > 0) {
                response.put("msg", params[0]);
            }
        } catch (Exception ex) {

        }
        return response;
    }
}
