package com.cm7.sim.console.security;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.saml.SAMLEntryPoint;
import org.springframework.security.saml.context.SAMLMessageContext;
import org.springframework.security.saml.websso.WebSSOProfileOptions;

public class MySAMLEntryPoint extends SAMLEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authenticationException) throws IOException {
        System.out.println("\u001B[33m mmm### 'mysaml entrypoint # commence' \u001B[0m");
        String accept = request.getHeader("Accept");
        boolean isAjax = accept != null && accept.contains("application/json");
        if (isAjax) {
            String result = createFailedJSONResponse("notlogin").toString();
            response.getWriter().write(result);
            response.setContentType("application/json");
        } else
            response.sendRedirect("/console/op/login");
    }

    @Override
    protected WebSSOProfileOptions getProfileOptions(SAMLMessageContext samlMessageContext,
            AuthenticationException authenticationException) throws MetadataProviderException {
        return super.getProfileOptions(samlMessageContext, authenticationException);
    }

    private static JSONObject createFailedJSONResponse(String msg) {
        try {
            JSONObject response = new JSONObject();
            response.put("status", "false");
            response.put("msg", msg);
            return response;
        } catch (Exception ex) {
            return null;
        }
    }

}
