package com.cm7.sim.console.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name="cm8-service", url="http://localhost:9090/api/console")
public interface Cm8Api {

    @GetMapping("/swagger/minh/test/generate-token")
    String getToken();

    @PostMapping("/swagger/minh/test/post-token")
    String postToken(@RequestBody String token);
}
