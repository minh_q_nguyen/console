package com.cm7.sim.console.service.impl;

import com.mongodb.BasicDBObject;
import org.springframework.mail.javamail.JavaMailSenderImpl;

public class MyMailSender extends JavaMailSenderImpl {

    private String from;
    private String apiKey;
    private boolean useApiKey;

    public MyMailSender(BasicDBObject emailConfig) {
        super();
        if (emailConfig != null && emailConfig.size() > 0) {
            boolean enabled = emailConfig.getBoolean("enabled");
            this.apiKey = emailConfig.getString("apiKey", "");
            this.from = emailConfig.getString("from", "noreply");
            this.setHost(emailConfig.getString("host"));
            this.setPort(emailConfig.getInt("port"));
            this.setUsername(emailConfig.getString("username", ""));
            this.setPassword(emailConfig.getString("password", ""));
        }
    }

    public String getFrom() {
        return this.from;
    }

    public String getApiKey() {
        return this.apiKey;
    }

    public boolean isUseApiKey() {
        return useApiKey;
    }

}
