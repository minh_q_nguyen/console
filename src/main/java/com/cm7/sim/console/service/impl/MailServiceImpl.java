package com.cm7.sim.console.service.impl;

import java.util.Map;
import javax.mail.internet.MimeMessage;
import com.cm7.sim.console.config.SmtpConfig;
import com.cm7.sim.console.email.ObjectEmail;
import com.cm7.sim.console.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    SmtpConfig smtpConfig;

    @Override
    public void sendEmail(ObjectEmail mail) throws Exception {
        var mailSender = smtpConfig.getMailSender();
        var msg = new MimeMessage(mailSender.getSession());

        MimeMessageHelper helper = new MimeMessageHelper(msg, true, "UTF-8");
        helper.setFrom(mailSender.getFrom());
        helper.setTo(mail.getReceivers());
        helper.setSubject(mail.getSubject());
        helper.setText(mail.getContent(), true);

        if (mail.getImagesInline() != null) {
            Map<String, String> imagesInline = mail.getImagesInline();
            imagesInline.forEach((imageName, path) -> {
                try {
                    helper.addInline(imageName, ResourceLoader.getResourceFile(path));
                }
                catch (Exception e) {
                    System.err.println("Error creating Mime message: " + e.getMessage());
                }
            });
        }

        mailSender.send(msg);
    }

}
