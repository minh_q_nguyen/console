package com.cm7.sim.console.service.impl;

import com.cm7.sim.console.feign.Cm8Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Cm8Service {

    @Autowired
    Cm8Api cm8Api;

    public String getToken() {
        return cm8Api.getToken();
    }

    public String postToken() {
        return cm8Api.postToken("abc token test");
    }
}
