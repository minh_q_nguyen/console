package com.cm7.sim.console.service.impl;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.cm7.sim.console.service.AccountService;
import com.cm7.sim.domain.model.SimAccountDto;
import com.cm7.sim.domain.model.request.LoginRequest;
import com.cm7.sim.domain.repository.AccountSessionRepository;
import com.cm7.sim.domain.repository.SimAccountRepository;
import com.cm7.sim.domain.repository.entity.SimAccount;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    private final SimAccountRepository simAccountRepository;
    private final AccountSessionRepository accountSessionRepository;

    public AccountServiceImpl(SimAccountRepository simAccountRepository,
        AccountSessionRepository accountSessionRepository) {

        this.simAccountRepository = simAccountRepository;
        this.accountSessionRepository = accountSessionRepository;
    }

    @Override
    public SimAccount createAccount(SimAccountDto req) {
        SimAccount ent = new SimAccount();
        ent.setEmail(req.getEmail());
        ent.setUsername(req.getUsername());
        ent.setPassword(req.setPassword());
        return simAccountRepository.save(ent);
    }

    @Override
    public String login(LoginRequest req, HttpSession session, HttpServletRequest httpRequest) {
        var account = simAccountRepository.findByEmail(req.getEmail());
        if (!account.isPresent()) {
            return "invalid email or password";
        }
        var matchedPassword =
            account.map(SimAccount::getPassword).filter(req.getPassword()::equals).orElse(null);
        if (StringUtils.isBlank(matchedPassword)) {
            return "invalid email or password";
        }
        var accountId = account.get().getId();
        var auth = new UsernamePasswordAuthenticationToken(accountId, "password",
            List.of(new SimpleGrantedAuthority("ROLE_ADMIN")));

        accountSessionRepository.findByAccountId(accountId).ifPresentOrElse(
            a -> accountSessionRepository.updateSession(accountId, session.getId()),
            () -> accountSessionRepository.createNewSession(accountId, session.getId()));

        session.setAttribute("email", account.get().getEmail());
        SecurityContextHolder.getContext().setAuthentication(auth);
        System.out.println("\u001B[33m mmm### 'security context set authentication:'" + auth + "' \u001B[0m");
        return "login successfully";
    }

    @Override
    public String deleteAccountById(String id) {
        var deletedAccount = simAccountRepository.findOne(id);
        if (deletedAccount == null) {
            return String.format("could not find account by id '%s'", id);
        }
        simAccountRepository.deleteAccountByIds(List.of(id));
        accountSessionRepository.deleteByAccountId(deletedAccount.getId());
        return String.format("deleted '%s'", id);
    }

}
