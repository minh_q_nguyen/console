package com.cm7.sim.console.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class AwsMessageQueueService {

    @Autowired
    QueueMessagingTemplate queueMessagingTemplate;

    public void sendMessageCallback() {
        queueMessagingTemplate.convertAndSend("queueName", "payload");
    }

}
