package com.cm7.sim.console.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.cm7.sim.domain.model.SimAccountDto;
import com.cm7.sim.domain.model.request.LoginRequest;
import com.cm7.sim.domain.repository.entity.SimAccount;

public interface AccountService {

    public SimAccount createAccount(SimAccountDto req);

    public String login(LoginRequest req, HttpSession session, HttpServletRequest httpRequest);

    public String deleteAccountById(String id);
}
