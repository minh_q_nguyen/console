package com.cm7.sim.console.service;

import java.util.Optional;
import com.cm7.sim.domain.repository.entity.SimAccount;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SimAccountRepository extends MongoRepository<SimAccount, String> {

    Optional<SimAccount> findByEmail(String email);

}
