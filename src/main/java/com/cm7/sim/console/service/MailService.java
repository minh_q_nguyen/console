package com.cm7.sim.console.service;

import com.cm7.sim.console.email.ObjectEmail;

public interface MailService {

    void sendEmail(ObjectEmail objectEmail) throws Exception;

}
