package com.cm7.sim.console.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class ResourceLoader {

	public static Resource getResourceFile(String resourceFile) throws UnsupportedEncodingException {
		return new ClassPathResource(resourceFile);
	}
}
