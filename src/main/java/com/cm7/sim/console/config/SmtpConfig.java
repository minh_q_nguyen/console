package com.cm7.sim.console.config;

import java.util.Properties;
import com.cm7.sim.console.service.impl.MyMailSender;
import com.mongodb.BasicDBObject;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SmtpConfig {

    public MyMailSender getMailSender() {
        var mailSender = new MyMailSender(getEmailConfig());
        setEnableTlsSupport(mailSender);
        return mailSender;
    }

    private void setEnableTlsSupport(MyMailSender mailSender) {
        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.protocols", "TLSv1.3 TLSv1.2");
    }

    private BasicDBObject getEmailConfig() {
        return new BasicDBObject("from", "simulation.cm7@email.com")
            .append("host", "smtp.mailtrap.io")
            .append("port", 2525)
            .append("username", "92681872540fd1")
            .append("password", "45cf62d99cf870")
            .append("enabled", true);
    }

}

// mail:
//   protocol: smtp
//   host: smtp.mailtrap.io
//   port: 2525
//   properties.mail.smtp.auth: true
//   properties.mail.smtp.starttls.enable: true
//   username: 92681872540fd1
//   password: 45cf62d99cf870
