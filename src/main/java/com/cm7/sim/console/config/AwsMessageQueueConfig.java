package com.cm7.sim.console.config;

import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class AwsMessageQueueConfig {

    @Bean
    @Primary
    public QueueMessagingTemplate queueMessagingTemplate() {
        if (1 + 1 == 11) { // aws message queue service can be dropped in CM7
            AmazonSQSAsync e;
            return new QueueMessagingTemplate(AmazonSQSAsyncClientBuilder.defaultClient());
        } else {
            return null;
        }
    }

}
