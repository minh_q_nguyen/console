package com.cm7.sim.console.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import com.cm7.sim.domain.config.CustomObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.feign.FeignAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import feign.Response;
import feign.codec.ErrorDecoder;

@Configuration
@EnableFeignClients("com.cm7.sim.console.feign")
@ImportAutoConfiguration({ FeignAutoConfiguration.class })
public class FeignConfig {

    @Autowired
    CustomObjectMapper customObjectMapper;

    @Bean
    public HttpMessageConverters httpMessageConverters() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(customObjectMapper);
        return new HttpMessageConverters(false, Arrays.asList(converter, new StringHttpMessageConverter()));
    }

    @Bean
    public MyErrorDecoder myErrorDecoder() {
        return new MyErrorDecoder();
    }

    public static class MyErrorDecoder implements ErrorDecoder {

        private final ErrorDecoder defaultErrorDecoder = new ErrorDecoder.Default();

        @Override
        public Exception decode(String methodKey, Response response) {
            if (response.status() >= 400) {
                String error = "{}";
                try {
                    error = getString(response.body().asInputStream());
                }
                catch (IOException e) {
                    System.err.println("IOException when decode: " + e.getMessage());
                }

                return new RuntimeException(error);
            }
            return defaultErrorDecoder.decode(methodKey, response);
        }
    }

    static String getString(InputStream is) {
        BufferedReader rd = null;
        StringBuilder res = new StringBuilder();
        try {
            rd = new BufferedReader(new InputStreamReader(is, "UTF-8"));

            String line = "";
            while ((line = rd.readLine()) != null) {
                res.append(line);
            }
        }
        catch (Exception e) {

        }
        finally {
            try {
                if (rd != null)
                    rd.close();
            }
            catch (Exception e) {

            }
        }

        return res.toString();
    }
}
